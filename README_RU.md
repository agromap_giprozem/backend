# Backend

## Введение

Этот проект предназначен для определения контуров, культур и урожайности земельных участков.

## Основные функции

- Определение контуров, культур и урожайности.
- Загрузка космических снимков с использованием sentinel2_downloader.
- Вычисление индексов для контуров с использованием космических снимков.
- Определение высоты над уровнем моря в Кыргызстане с использованием elevation-Kyrgyzstan.
- Управление данными по регионам, районам и земельным участкам.
- Отчетность.

## Технологии

- Django: основной фреймворк веб-приложения.
- Python3.10: основной язык программирования.
- GDAL: библиотека для обработки геопространственных данных.
- Docker/Docker Compose: для контейнеризации приложения и его зависимостей.
- PyTorch/YOLOv8: для обучения и применения моделей машинного обучения для определения контуров, культур и урожайности.
- sentinel2_downloader: для загрузки космических снимков.
- elevation-Kyrgyzstan: для определения высоты над уровнем моря в Кыргызстане.

## Использование

Пожалуйста, обратитесь к файлу [USAGE_RU.md](USAGE_RU.md) для подробной информации об использовании.

## Для разработчиков

Для более подробной информации о ключевых компонентах проекта и скриптах, используемых в разработке,
смотрите [здесь](DEVELOPER_RU.md).

## Авторы

### Главные разработчики

- **Azimkozho**
    * GitHub: [Azim-Kenzh](https://github.com/Azim-Kenzh)
    * Email: <azimkozho.developer@gmail.com>


- **Chynybai**
    * GitHub: [TIP-ROK](https://github.com/TIP-ROK)
    * Email: <chenye797@gmail.com>


- **Daniel**
    * GitHub: [Daniel](https://github.com/azamatdaniel0)
    * Email: <azamatdaniel0@gmail.com>


- **Almaz**
    * GitHub: [Almaz](https://github.com/sharshenaliev)
    * Email: <almaztestdjango@gmail.com>

### Второстепенные разработчики

- **Aibek**
    * GitHub: [Aibek](https://github.com/RikiTwiki)
    * Email: <aibekrenadov@gmail.com>


- **Daniil**
    * GitHub: [Daniil](https://github.com/daniilpopoff)
    * Email: <daniil.popoff01@gmail.com>


- **Meerim**
    * GitHub: [Meerim](https://github.com/meerimkanybekova)
    * Email: <kanybekova_m@auca.kg>


- **Feliks**
    * GitHub: [Feliks](https://github.com/feliksKdm)
    * Email: <altymysovfeliks@gmail.com>


- **Nursultan**
    * GitHub: [Nursultan](https://github.com/Nursmen)
    * Email: <nnursultan07@mail.ru>

- **Kydyrbek**
    * GitHub: [Kydyrbek](https://github.com/Kydyrbek97)
    * Email: <Kydyrbek.Mamatkulov@gmail.com>
