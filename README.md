# Backend

## Introduction

This project is designed for the identification of contours, crops, and crop yield of land plots.

## Key Features

- Identification of contours, crops, and crop yield.
- Downloading of satellite images using sentinel2_downloader.
- Calculation of indices for contours using satellite images.
- Determination of the elevation above sea level in Kyrgyzstan using elevation-Kyrgyzstan.
- Management of data by regions, districts, and land plots.
- Reporting.

## Technologies

- Django: The primary web application framework.
- Python3.10: The main programming language.
- GDAL: A library for processing geospatial data.
- Docker/Docker Compose: For containerizing the application and its dependencies.
- PyTorch/YOLOv8: For training and applying machine learning models for the identification of contours, crops, and crop
  yield.
- sentinel2_downloader: For downloading satellite images.
- elevation-Kyrgyzstan: For determining the elevation above sea level in Kyrgyzstan.

## Usage

Please refer to the [USAGE.md](USAGE.md) file for detailed information on usage.

## For Developers

For more detailed information about key components of the project and scripts used in development,
see [here](DEVELOPER.md).

## Authors

### Main Developers

- **Azimkozho**
    * GitHub: [Azim-Kenzh](https://github.com/Azim-Kenzh)
    * Email: <azimkozho.developer@gmail.com>


- **Chynybai**
    * GitHub: [TIP-ROK](https://github.com/TIP-ROK)
    * Email: <chenye797@gmail.com>


- **Daniel**
    * GitHub: [Daniel](https://github.com/azamatdaniel0)
    * Email: <azamatdaniel0@gmail.com>


- **Almaz**
    * GitHub: [Almaz](https://github.com/sharshenaliev)
    * Email: <almaztestdjango@gmail.com>

### Secondary Developers

- **Aibek**
    * GitHub: [Aibek](https://github.com/RikiTwiki)
    * Email: <aibekrenadov@gmail.com>


- **Daniil**
    * GitHub: [Daniil](https://github.com/daniilpopoff)
    * Email: <daniil.popoff01@gmail.com>


- **Meerim**
    * GitHub: [Meerim](https://github.com/meerimkanybekova)
    * Email: <kanybekova_m@auca.kg>


- **Feliks**
    * GitHub: [Feliks](https://github.com/feliksKdm)
    * Email: <altymysovfeliks@gmail.com>


- **Nursultan**
    * GitHub: [Nursultan](https://github.com/Nursmen)
    * Email: <nnursultan07@mail.ru>

- **Kydyrbek**
    * GitHub: [Kydyrbek](https://github.com/Kydyrbek97)
    * Email: <Kydyrbek.Mamatkulov@gmail.com>
