# Использование

Здесь приведены основные инструкции по использованию приложения `agromap_backend`.

## Настройка переменных окружения

Для корректной работы приложения вам нужно настроить следующие переменные окружения в файле `.env`:

Заполните `Ваше значение` соответствующими значениями, которые вы хотите использовать для каждой переменной.

```shell
POSTGRES_DB=Ваше значение
POSTGRES_USER=Ваше значение
POSTGRES_PASSWORD=Ваше значение
POSTGRES_HOST=Ваше значение
POSTGRES_PORT=Ваше значение
ALLOWED_HOSTS=Ваше значение
SECRET_KEY=Ваше значение
SCI_HUB_USERNAME_V2=Ваше значение
SCI_HUB_PASSWORD_V2=Ваше значение
```

## Запуск приложения

1. Сначала вы должны клонировать репозиторий на свою машину. Если вы еще этого не сделали, выполните следующую команду:

    ```
    git clone https://gitlab.com/agromap_giprozem/backend.git
    ```

2. Перейдите в папку проекта:

    ```
    cd backend
    ```

3. Запустите Docker Compose:

    ```
    docker-compose up -d --build
    ```

   Это запустит все необходимые сервисы, указанные в вашем `docker-compose.yml`, включая ваше Django приложение, базу
   данных PostGIS и другие.

## Работа с приложением

1. Для входа в приложение откройте браузер и перейдите по адресу `http://localhost:8111`.

2. Здесь вы найдете интерфейс вашего приложения и сможете начать работу.


